#ddup
该项目主要用于协作Study Group的学习记录和相关代码。

# 成员
* Hinsteny
* Esther
* Divers
* GavinCook

## 第一期 Netty
Netty提供异步的、事件驱动的网络应用程序框架和工具，用以快速开发高性能、高可靠性的网络服务器和客户端程序。

学习目标（以及需要解决的问题）：
* IO（包括BIO,AIO,NIO）相关，各自的优缺点、使用场景以及相关api的熟练使用
* http、udp协议深入了解
* 多线程的使用场景，该如何使用
* Netty是如何做到高效、高性能的？
* 事件驱动的概念、场景以及如何实现
* netty对堆外内存如何管理？
* netty的编码器（链）、解码器（链）是如何协作的？
* netty任务调度器（Hashed wheel timer）实现机制，[链接](http://netty.io/wiki/using-as-a-generic-library.html#hashed-wheel-timer)

...

### 第一周 不同类型IO的概念以及相关Java示例
* 分享人： Hinsteny
* 时间： 2016-05-25
* 要点： 阻塞、非阻塞是在IO连接建立阶段；同步、异步则是在IO操作阶段。扩展队列的阻塞和同步。
* 分享整理： http://blog.csdn.net/HinstenyHisoka/article/details/51492195

### 第二周 ByteBuf vs ByteBuffer
* 分享人：Gavincook
* 时间：2016-05-31
* 要点： ByteBuffer和ByteBuf的工作机制对比;堆外内存的使用分析;ByteBuf的优势
* 分享整理： https://www.zybuluo.com/gavincook/note/393325

### 第三周 事件驱动的概念、场景以及相关示例
* 分享人：Divers
* 时间：2016-06-8
* 要点： 事件驱动的概念、场景以及相关示例
* 分享整理： http://note.youdao.com/yws/public/redirect/share?id=764d566357a73a24a56ae241e61553a5&type=false

### 第四周 堆栈算法和java集合堆栈浅析
* 分享人：Esther
* 时间：2016-06-19
* 要点：*因意外取消*
* 分享整理： 

### 第五周 依据HTTPS中TLS/SSL加密原理衍生的数据加密实现
* 分享人：Hinsteny
* 时间：2016-06-26
* 要点：HTTPS协议的原理,在SpringMVC环境下示例代码
* 分享整理： http://blog.csdn.net/HinstenyHisoka/article/details/51761862


